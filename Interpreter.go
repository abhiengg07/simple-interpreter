package main

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

type token struct {
	Name  string
	Value string
}

type Self struct {
	Token     *token
	Str_Array []string
	Pos       int
	Text      string
}

func IsDigit(s string) bool {
	if _, err := strconv.ParseFloat(s, 64); err == nil {
		return true
	}
	return false
}

func Error(err error) {
	log.Fatal(err)
}

func Tokenizer(value string) *token {
	t := new(token)
	if IsDigit(value) {
		t.Value = value
		t.Name = "NUMBER"
	} else {
		switch value {
		case "+":
			t.Name = "PLUS"
		case "-":
			t.Name = "MINUS"
		case "*":
			t.Name = "MUL"
		case "/":
			t.Name = "DIV"
		case "(":
			t.Name = "LPAREN"
		case ")":
			t.Name = "RPAREN"
		}
	}
	return t
}

func (self *Self) next() {
	if self.Pos > len(self.Str_Array)-1 {
		t := new(token)
		t.Name = "EOF"
		t.Value = "NONE"
		self.Token = t
	} else {
		current_str := self.Str_Array[self.Pos]
		t := Tokenizer(current_str)
		self.Pos += 1
		self.Token = t
	}
}

func numConstruct(key int, str_slice []string) (num string, i int) {
	num = str_slice[key]
	for i := key + 1; i <= len(str_slice)-1; i++ {
		if IsDigit(str_slice[i]) {
			num += str_slice[i]
		} else {
			return num, i
		}
	}
	return num, i
}

func (self *Self) lexer() {
	self.Pos = 0
	f, err := os.Open(self.Text)
	if err != nil {
		Error(err)
	}
	defer f.Close()
	reader := bufio.NewReader(f)
	input, err := ioutil.ReadAll(reader)
	if err != nil {
		Error(err)
	}
	str_slice := strings.Split(string(input), "")
	for i := 0; i <= len(str_slice)-1; i++ {
		if IsDigit(str_slice[i]) {
			value, j := numConstruct(i, str_slice)
			i = (j - 1) // setting the loop count to the new one after number concatenation
			self.Str_Array = append(self.Str_Array, value)
		} else if str_slice[i] == " " {
		} else {
			self.Str_Array = append(self.Str_Array, str_slice[i])
		}
	}
}

func (self *Self) factor() (result float64) {
	token := self.Token
	if token.Name == "NUMBER" {
		self.next()
		num, _ := strconv.ParseFloat(token.Value, 64)
		result = num
	} else if token.Name == "LPAREN" {
		self.next()
		result = self.expr()
		self.next()
	}
	if token.Name == "" {
		return
	}
	return
}

func (self *Self) term() (result float64) {
	result = self.factor()
	for self.Token.Name == "MUL" || self.Token.Name == "DIV" {
		token := self.Token
		if token.Name == "MUL" {
			self.next()
			result = result * self.factor()
		} else if token.Name == "DIV" {
			self.next()
			result = result / self.factor()
		}
	}
	return result
}
func (self *Self) expr() (result float64) {
	result = self.term()
	for self.Token.Name == "PLUS" || self.Token.Name == "MINUS" {
		token := self.Token
		self.next()
		result = self.term()
		if token.Name == "PLUS" {
			self.next()
			result = result + self.term()
		} else if token.Name == "MINUS" {
			self.next()
			result = result - self.term()
		}
	}
	return result
}

func (self *Self) interpreter() (result float64) {
	self.lexer()
	self.next()
	result = self.expr()
	return result
}

func main() {
	self := new(Self)
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("--------------------Interpreter--------------------")
	fmt.Print("$ ")
	for scanner.Scan() {
		//Client Text Input
		filename := strings.Split(scanner.Text(), ".")
		if filename[1] == "s" {
			self.Text = scanner.Text()
		} else {
			Error(errors.New("Please specify a proper filename with (.s) extension"))
		}
		// Pos is the index into the string array obtained from the client text input
		result := self.interpreter()
		fmt.Println("=> ", result)
		fmt.Print("$ ")
	}
}
